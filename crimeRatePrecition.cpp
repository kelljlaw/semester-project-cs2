#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <vector>

using namespace std;

using CrimeData = vector<int>;

class CrimeRate {
    protected:
    string city;
    // vector<CrimeData> crimedata;
    // vector<CrimeRate> year_crime;
    // int population = 0;
    // int numCrimes = 0;

    public:
    void getFromUser() {
        cout << "Enter the city you want to view: " << endl;
        cout << "City options are: Denver, Aurora, Colorado Springs, Boulder, Fort Collins, Grand Junction, Littleton, Greeley, Arvada, Pueblo." << endl;
        cin >> city;

        //if(city != "Denver" || "Aurora" || "Colorado Springs" || "Boulder" || "Fort Collins" || "Grand Junction" || "Littleton" || "Greeley "|| "Arvada" || "Pueblo") {
        // if(city != "Denver") {    
        //     cout << "Error: No data on the entered city." << endl;
        // }
    }
    void readInput() {
        ifstream crimeRate;
        crimeRate.open("crimeRatesDenver.txt");
        // crimeRate >> numCrimes;

        if(! crimeRate) {
            cout << "There was an error opening this file." << endl;
        }
        // while(getline(crimeRate, city)) {
        //     getFromUser();
        //     year_rate.push_back(city);
        //     break;
        // }
        vector<CrimeData> crimedata; {
            string first_line;
            getline(crimeRate, first_line);
            istringstream iss(first_line);
            int element;
            while(iss >> element) {
              crimedata.push_back(CrimeData());
                crimedata.back().push_back(element);
            }
            bool end = false;
            while(! end) {
                for(int i = 0; i < crimedata.size(); i++) {
                    int element;
                    if (crimeRate >> element) {
                        crimedata[i].push_back(element);
                    }
                    else {
                        end = true;
                        break;
                    }
                }  
            }
            for(size_t i = 0; i < crimedata.size(); i++) {
                for(int element : crimedata[i]) {
                    cout << element << endl;
                }
            }
        }

    }
    // friend ostream & operator << (ostream &out, const CrimeRate &list) {
    //     for(int i = 0; i< list.crimedata; i++) {
    //         out << crimedata[i] << endl;
    //     }
    //     return out;
    // }
};

class BestFit {
    vector<float> year;
    vector<float> rate;

    float slope;
    float constant;

    float sum_year_slope;
    float sumYear;
    float sumRate;

    float sumYearSqu;
    float sumRateSqu;

    public:
    BestFit(){
        slope = 0;
        constant = 0;
        sum_year_slope = 0;
        sumYear = 0;
        sumRate = 0;
        sumYearSqu = 0;
        sumRateSqu = 0;
    }
    void calcSlope() {
        float M = year.size();
        float temp1 = (M * sum_year_slope - sumYear * sumRate);
        float temp2 = (M * sumYearSqu - sumYear * sumYear);
        
        slope = temp1/temp2;
    }
    void calcConstant() {
        float b = year.size();
        float temp1 = (sumRate * sumYearSqu - sumYear * sum_year_slope);
        float temp2 = (b * sumYearSqu - sumYear * sumYear);
        
        constant = temp1/temp2;
    }
    int dataSize() {
        return year.size();
    }
    float Slope() {
        if(slope == 0) {
            calcSlope();
        }
        return slope;
    }
    float Constant() {
        if(constant == 0) {
            calcConstant();
        }
        return constant;
    }
    void line() {
        if (slope == 0 && constant == 0) {
            calcSlope();
            calcConstant();
        }
        cout << "The model best representing the trend in the these crime rates is: " << slope << "x + " << constant << endl;
    }
};


int main()
{
    CrimeRate cr;

    cr.readInput();

    return 0;
}