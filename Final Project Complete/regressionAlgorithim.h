#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <vector>

// #include "crimeRateInput.h"

using namespace std;
 
class CrimeRate {
    protected:
    vector<string> strYear;
    vector<string> strRate;
    vector<double> year;
    vector<double> rate;
    string city;
    int category;

    public:
    CrimeRate(int newCategory = 0, string newCity = " ") {
        category = newCategory;
        city = newCity;
    }
    void getCategory() {
        cout << "Welcome to the Colorado Crime Rate Predictor!" << endl;
        cout << "Please select a crime category (Enter 1-4): " << endl;
        cout << "[1] Violent Crime" << endl;
        cout << "[2] Murder" << endl;
        cout << "[3] Aggravated Assault" << endl;
        cout << "[4] Property Crime" << endl;
        cin >> category;

        ifstream crimeRates;
        if(category == 1) { 
            crimeRates.open("ViolentCrime.txt"); 
            // cout << "Violent crime opened successfully " << endl;
        } else if(category == 2) { 
            crimeRates.open("Murder.txt"); 
            // cout << "Murder opened successfully " << endl;
        } else if(category == 3) { 
            crimeRates.open("AggravatedAssualt.txt"); 
            // cout << "Aggravated Assault opened successfully " << endl;
        } else if(category == 4) { 
            crimeRates.open("PropertyCrime.txt"); 
            // cout << "Property Crime opened successfully " << endl;    
        } 
        if (!crimeRates) {
            cout << "There was an error opening this file." << endl;
            exit(1);
        }
       
        getCity();
        // cout << city << endl;
        while(crimeRates.good()) {
            string line;
            while(getline(crimeRates, line)) {
                if(line.find("Year") != string::npos) {
                    // cout << line << endl;
                    tokenizeYear(line);
                    // printY(Year);
                    // cout << endl;
                    strYear.erase(strYear.begin());
                    // printY(strYear);
                    // cout << endl;
                }
                if(line.find(city) != string::npos) {
                    // cout << line << endl;
                    line.find(city);
                    tokenizeRate(line);
                    // printR(Rate);
                    // cout << endl;
                    strRate.erase(strRate.begin());
                    // printR(strRate);
                    // cout << endl;
                }
            }
        }
        convertStrToVec();
    }
    void getCity() {
        int citySelection;
        cout << "Please select the city you want to see data from: " << endl;
        cout << "City options: " << endl;
        cout << "[1] Arvada" << endl;
        cout << "[2] Aurora" << endl;
        cout << "[3] Boulder" << endl;
        cout << "[4] Colorado Springs" << endl;
        cout << "[5] Denver" << endl;
        cout << "[6] Fort Collins" << endl;
        cout << "[7] Grand Junction" << endl;
        cout << "[8] Greeley" << endl;
        cout << "[9] Littleton" << endl;
        cout << "[10] Pueblo" << endl;
        cin >> citySelection;
        if(citySelection == 1) {
            city = "Arvada";
        } else if (citySelection == 2) {
            city = "Aurora";
        } else if(citySelection == 3) {
            city = "Boulder";
        } else if(citySelection == 4) {
            city = "Colorado_Springs";
        } else if(citySelection == 5) {
            city = "Denver";
        } else if(citySelection == 6) {
            city = "Fort_Collins";
        } else if(citySelection == 7) {
            city = "Grand_Junction";
        } else if(citySelection == 8) {
            city = "Greeley";
        } else if(citySelection == 9) {
            city = "Littleton";
        } else if(citySelection == 10) {
            city = "Pueblo";
        }
    }
    void printY(vector<string> &strYear){
        for(int i = 0; i < strYear.size(); i++) {
            cout << strYear.at(i) << ' ';
        }
    }
    void printR(vector<string> &strRate){
        for(int i = 0; i < strRate.size(); i++) {
            cout << strRate.at(i) << ' ';
        }
    }
    void tokenizeYear(string s) {
        stringstream ss(s);
        string word;
        while(ss >> word) {
            // cout << word << endl;
            strYear.push_back(word);
        }
    }
    void tokenizeRate(string s) {
        stringstream ss(s);
        string word;
        while(ss >> word) {
            // cout << word << endl;
            strRate.push_back(word);
        }
    }
    void convertStrToVec() {
        for(int i = 0; i < strYear.size(); i++) {
            year.push_back(stod(strYear[i]));
        }
        cout << "Years: ";
        printy(year);
        cout << endl;
        for(int i = 0; i < strRate.size(); i++) {
            rate.push_back(stod(strRate[i]));
        }
        cout << "Rates: ";
        printr(rate);
        cout << endl;
    }
    void printy(vector<double> &year){
        for(int i = 0; i < year.size(); i++) {
            cout << year.at(i) << ' ';
        }
    }
    void printr(vector<double> &rate){
        for(int i = 0; i < rate.size(); i++) {
            cout << rate.at(i) << ' ';
        }
    }
};

class BestFit: public CrimeRate {
    protected:

    double slope;
    double constant;

    double sum_year_slope;
    double sumYear;
    double sumRate;

    double sumYearSqu;
    double sumRateSqu;

    public:
    BestFit(){
        slope = 0.0;
        constant = 0.0;
        sum_year_slope = 0.0;
        sumYear = 0.0;
        sumRate = 0.0;
        sumYearSqu = 0.0;
        sumRateSqu = 0.0;
    }
    void calcSlope(vector<double>&year, vector<double>&rate) {
        double M = year.size();
        double temp1 = (M * sum_year_slope - sumYear * sumRate);
        double temp2 = (M * sumYearSqu - sumYear * sumYear);
        
        slope = temp1/temp2;
    }
    void calcConstant(vector<double>&year, vector<double>&rate) {
        double b = year.size();
        double temp1 = (sumRate * sumYearSqu - sumYear * sum_year_slope);
        double temp2 = (b * sumYearSqu - sumYear * sumYear);
        
        constant = temp1/temp2;
    }
    int dataSize(vector<double>&year) {
        return year.size();
    }
    double Slope(vector<double>&year, vector<double>&rate) {
        if(slope == 0) {
            calcSlope(year, rate);
        }
        return slope;
    }
    double Constant(vector<double>&year, vector<double>&rate) {
        if(constant == 0) {
            calcConstant(year, rate);
        }
        return constant;
    }
    void line() {
        if (slope == 0 && constant == 0) {
            calcSlope(year, rate);
            calcConstant(year, rate);
        }
        cout << "The model best representing the trend in the these crime rates is: " << endl;
        cout << slope << "x + " << constant << endl;
    }
    void getData(int d) {
        for(int i = 0; i < d; i++) {
            char comma;
            double x;
            double y;
            x = year[i];
            y = rate[i];
            sum_year_slope += x * y;
            sumYear += x;
            sumRate += y;
            sumYearSqu += x * x;
            sumRateSqu += y * y;
        }
    }
    double predict(double x) {
        return slope * x + constant;
    }
    double errorSquared() {
        double answer = 0;
        for(int i = 0; i < year.size(); i++) {
            answer += ((predict(year[i]) - rate[i]) * (predict(year[i]) - rate[i]));
        }
        return answer;
    }
};