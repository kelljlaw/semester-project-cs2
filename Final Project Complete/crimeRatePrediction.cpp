#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <vector>

#include "regressionAlgorithim.h"
// #include "crimeRateInput.h"

using namespace std;

int main()
{
    BestFit bestFit;
    bestFit.CrimeRate::getCategory();

    int n = 10;
    bestFit.getData(10);

    bestFit.line();
    
    char choice;
    cout << "Would you like to see the predicted crime rate from a specific year? Y/N" << endl;
    cin >> choice;
    int choiceYear;
    if(choice == 'Y') {
        cout << "Please enter a year. (Example: 2060)" << endl;
        cin >> choiceYear;
        cout << "Predicted value at " << choiceYear << " is " << bestFit.predict(choiceYear) << endl;
        // cout << "The errorSquared  = " << bestFit.errorSquared() << endl;
    }

    return 0;

}